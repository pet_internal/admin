import Login from 'pages/authentication/Login';
import React from 'react';
import { Redirect } from 'react-router-dom';
import Loadable from 'components/Loadable';
import { lazy } from 'react';

const UserPage = Loadable(lazy(() => import('pages/user')));

const authProtectedRoutes = [
  { path: '/users', component: UserPage },
  {
    path: '/',
    exact: true,
    component: () => <Redirect to="/users" />
  }
];

const publicRoutes = [{ path: '/login', component: Login }];

export { authProtectedRoutes, publicRoutes };
